import React, { Component } from 'react'
import PropTypes from 'prop-types';
import CoursesList from './CoursesList'
import FormCourse from './courseForm'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            courses: [
                { id: 1, name: 'React desde cero', teacher: 'Jorge G' },
                { id: 2, name: 'Drupal 8', teacher: 'Calamardo' }
            ]
        }
        this.handleOnAddCourser = this.handleOnAddCourser.bind(this)
    }

    handleOnAddCourser(e) {
        console.log('Evento accionado en react');
        e.preventDefault()

        let target = e.target,
            form = {
                id: target.id.value,
                name: target.name.value ? target.name.value : App.defaulProps.name,
                teacher: target.teacher.value ? target.teacher.value : App.defaulProps.teacher
            }
        this.setState({
            courses: this.state.courses.concat([form])
        })
        target.reset()
    }

    render() {
        return (
            <div>
                <h1>Cursos desde casa</h1>
                <FormCourse onAddCourse={this.handleOnAddCourser} />
                <CoursesList courses={this.state.courses} />
            </div>
        )
    }
}

App.propTypes = {
    name: PropTypes.string.isRequired,
    teacher: PropTypes.string.isRequired
};

App.defaulProps = {
    name: 'Curso desconocido',
    teacher: 'Sin asignar',
}


export default App