import React from 'react'

const FormCourse = (props) => (
    <form onSubmit={props.onAddCourse}>
        <input type="text" name="name" id="name" placeholder='Nombre del curso' />
        <input type="text" name="teacher" id="teacher" placeholder="Profesor" />
        <input type="hidden" name="id" value={Math.floor(Math.random() * 100)} />
        <input type="submit" value="Guardar" />
    </form>
)

export default FormCourse